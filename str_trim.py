people = []

with open("data/people.txt") as people_file:
    for line in people_file.readlines():
        print("Reading next line")
        print("Line without strip()")
        print(repr(line))

        # str.strip(), with no arguments, removes whitespace from both sides
        # of a string.
        stripped_line = line.strip()
        print("Line _with_ strip()")
        print(repr(stripped_line))
        people.append(stripped_line)
        print("")

print("People:")
for person in people:
    # Notice no extra \n (newline) character when printing person
    print(person)